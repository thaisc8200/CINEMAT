---
title: "El hijo"
title_link: el_hijo
trailer: "https://www.youtube.com/embed/a90TPlWvoRE"
vote_average: 7
popularity: 103.4
poster_path: "Hijo.jpg"
original_language: en
original_title: "Brightburn"
genre:
- "Ciencia Ficción"
- "Terror"
backdrop_path: "/uHEI6v8ApQusjbaRZ8o7WcLYeWb.jpg"
overview: "¿Qué pasaría si un niño de otro mundo aterrizara de emergencia en la
Tierra, pero en lugar de convertirse en un héroe para la humanidad fuera algo
mucho más siniestro?"
release_date: "2019-05-10"
duration: "90 min"
country: "Estados Unidos"
year: 2019
directors:
- "David Yarovesky"
actors:
- "Elisabeth Banks"
- "Jackson A. Dunn"
- "David Denman"
- "Steve Agee"
---

{% include pelis.html %}
