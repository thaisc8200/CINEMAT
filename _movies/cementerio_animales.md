---
title: "Cementerio de animales"
title_link: cementerio_animales
trailer: "https://www.youtube.com/embed/ghXOVshAyvM"
vote_average: 5.9
popularity: 85.04
poster_path: "CementerioAnimales.jpg"
original_language: en
original_title: "Pet Sematary"
genre:
- "Terror"
- "Suspense"
backdrop_path: "/dpJq6trUDOutSPSLrFdROPmzaVn.jpg"
overview: "El doctor Louis Creed (Clarke) se muda con su mujer Racher (Seimetz) y
  sus dos hijos pequeños de Boston a un pueblecito de Maine, cerca del nuevo hogar
  de la familia descubrirá una terreno misterioso escondido entre los árboles. Cuando
  la tragedia llega, Louis hablará con su nuevo vecino, Jud Crandall (Lithgow),
  desencadenando una peligrosa reacción en cadena que desatará un mal de horribles
  consecuencias."
release_date: "2019-04-04"
duration: "101 min"
country: "Estados Unidos"
year: 2019
directors:
- "Dennis Widmyer"
actors:
- "Amy Seimetz"
- "Hugo Lavoie"
- "Jason Clarke"
- "Jeté Laurence"
- "John Lithgow"
- "Lucas Lavoie"
---

{% include pelis.html %}
