---
title: "Dumbo"
title_link: dumbo
trailer: "https://www.youtube.com/embed/RhwjDoXxfKg"
vote_average: 6.6
popularity: 70.045
poster_path: "Dumbo.jpg"
original_language: en
original_title: "Dumbo"
genre:
- "Aventura"
- "Família"
- "Fantasía"
backdrop_path: "/5tFt6iuGnKapHl5tw0X0cKcnuVo.jpg"
overview: "La nueva recreación de la película DUMBO dirigida por Tim Burton cuenta
  con Colin Farrel como Holt Farrier quien con ayuda de sus hijos Milly (Nico Parker)
  y Joe (Finley Hobbins) se dedican a cuidar a un elefante recién nacido cuyas orejas
  gigantes le hacer ser el hazmerreír en un Circo que no pasa por su mejor momento. La
  familia circense de Holt incluye además a la señorita Atlantis (Sharon Rooney),
  Rongo (DeObia Oparei), Pramesh Singh (Roshan Seth) y su sobrino (Ragevan Vasan),
  La grandiosa Catherine (Zenaida Alcalde) y el magnífico Iván (Miguel Muñoz). Max
  Medici (Danny DeVito) dueño del circo, se decepciona al saber sobre las enormes
  orejas del pequeño paquidermo hasta que descubre que es capaz de volar, llevando
  al circo de regreso a la prosperidad."
release_date: "2019-03-27"
duration: "112 min"
country: "Estados Unidos"
year: 2019
directors:
- "Tim Burton"
actors:
- "Colin Farrell"
- "Danny Devito"
- "Eva Green"
- "Finley Hobbins"
- "Michael Keaton"
- "Nico Parker"
---

{% include pelis.html %}
