---
title: "Pequeño gran problema"
title_link: pequeno_gran_problema
trailer: "https://www.youtube.com/embed/fCshksCjRBU"
vote_average: 6.1
popularity: 102.116
poster_path: "PequeGranProblema.jpg"
original_language: en
original_title: "Little"
genre:
- "Comedia"
backdrop_path: "/tmM78qRhpg0i2Cky8Q8hXKASOXY.jpg"
overview: "Jordan Sanders es una mujer implacable que, con la seguridad que le da
  la riqueza con la que cuenta, no tiene ningún tipo de reparo en tratar a sus empleados
  como si fueran inservibles. La mayor víctima de este comportamiento es April,
  la dócil asistente de Jordan, que debe satisfacer las caprichosas necesidades
  de su jefa. Todo cambia cuando, un día, Jordan se despierta siendo una niña de
  nuevo."
release_date: "2019-04-04"
duration: "108 min"
country: "Estados Unidos"
year: 2019
directors:
- "Tina Gordon Chism"
actors:
- "Issa Rae"
- "Jd Mccrary"
- "Justin Hartley"
- "Marsai Martin"
- "Regina Hall"
- "Tone Bell"
---

{% include pelis.html %}
