---
title: "Hellboy"
title_link: hellboy
trailer: "https://www.youtube.com/embed/UEOPrqMPRZA"
vote_average: 5.1
popularity: 128.169
poster_path: "Hellboy.jpg"
original_language: en
original_title: "Hellboy"
genre:
- "Acción"
- "Fantasía"
- "Terror"
backdrop_path: "/5BkSkNtfrnTuKOtTaZhl8avn4wU.jpg"
overview: "La Agencia para la Investigación y Defensa Paranormal (AIDP) encomienda
a Hellboy la tarea de derrotar a un espíritu ancestral: Nimue, conocida como 'La
Reina de la Sangre'. Nimue fue la amante del mismísimo Merlín durante el reinado
del Rey Arturo, de él aprendió los hechizos que la llevaron a ser una de las brujas
más poderosas… Pero la locura se apoderó de ella y aprisionó al mago para toda
la eternidad. Hace siglos consiguieron acabar con esta villana, enterrándola profundamente,
pero ha vuelto de entre los muertos con la intención de destruir a la humanidad
con su magia negra.  Nueva película basada en el cómic 'Hellboy' de Mike Mignola,
que tendrá calificación R y se desmarcará de los films de Guillermo del Toro."
release_date: "2019-04-10"
duration: "120 min"
country: "Reino Unido"
year: 2019
directors:
- "Neil Marshall"
actors:
- "Daniel Dae Kim"
- "David Harbour"
- "Ian Mcshane"
- "Milla Jovovich"
- "Penelope Mitchell"
- "Sasha Lane"
---

{% include pelis.html %}
