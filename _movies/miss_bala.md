---
title: "Miss Bala"
title_link: miss_bala
trailer: "https://www.youtube.com/embed/e-kPf-n4Mto"
vote_average: 6.6
popularity: 56.793
poster_path: "MissBala.jpg"
original_language: en
original_title: "Miss Bala"
genre: "Acción"
backdrop_path: "/5OoR3aVpxXluSCe9P5mhqOa36Gb.jpg"
overview: "La ganadora de un concurso de belleza se ve obligada a trabajar para un
  jefe del crimen después de ser testigo de un asesinato. Una nueva versión de la
  película de 2011, 'Miss Bala', dirigida por Gerardo Naranjo y protagonizada por
  Stephanie Sigman."
release_date: "2019-02-01"
duration: "104 min"
country: "Estados Unidos"
year: 2019
directors:
- "Catherine Hardwicke"
actors:
- "Gina Rodriguez"
- "Anthony Mackie"
- "Matt Lauria"
- "Ismael Cruz Cordova"
---

{% include pelis.html %}
